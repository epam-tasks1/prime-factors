﻿using System;
using System.Collections.Generic;

namespace PrimeFactorsTask
{
    public static class PrimeFactors
    {
        public static int[] GetFactors(int number)
        {
            List<int> listOfPrimeNumbers = new List<int>();

            if (number <= 0)
            {
                throw new ArgumentException("number is less or equal zero.");
            }

            for (int i = 2; i <= number; i++)
            {
                if (number % i == 0)
                {
                    listOfPrimeNumbers.Add(i);
                    number /= i;

                    if (number == 1)
                    {
                        break;
                    }

                    i = 1;
                }
            }

            return listOfPrimeNumbers.ToArray();
        }
    }
}
